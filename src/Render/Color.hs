module Render.Color where

import Common

import Linear

black, white, red, green, blue, cyan, magenta, yellow :: V4 Float
black   = V4 0 0 0 1
white   = V4 1 1 1 1
red     = V4 1 0 0 1
green   = V4 0 1 0 1
blue    = V4 0 0 1 1
cyan    = V4 0 1 1 1
magenta = V4 1 0 1 1
yellow  = V4 1 1 0 1
