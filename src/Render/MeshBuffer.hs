module Render.MeshBuffer
    ( MeshBuffer, uploadMesh, renderMeshBuffer ) where

import Common
import Render.Mesh
import Render.Vertex
import Control.Monad.GL
import Graphics.Buffer
import Graphics.Freeable

data MeshBuffer a = NonIndexedBuffer (Buffer (Vertex a))
                  | IndexedBuffer (Buffer (Vertex a)) (Buffer Int)
                  deriving Eq

uploadMesh :: MonadGL m => Mesh Float -> m (MeshBuffer Float)
uploadMesh (NonIndexed tris) =
    NonIndexedBuffer <$> newVertexBuffer dat
    where dat = concatMap triangleToList tris
uploadMesh (Indexed verts inds) =
    IndexedBuffer <$> newVertexBuffer verts <*> newIndexBuffer inds

renderMeshBuffer :: MonadGL m => MeshBuffer a -> m () -> m ()
renderMeshBuffer (NonIndexedBuffer b) a = withBuffer ArrayBuffer b $ do
    a
    drawArrays Triangles 0 (b^.bufferSize)
renderMeshBuffer (IndexedBuffer vb ib) a = withBuffer ArrayBuffer vb $ withBuffer IndexBuffer ib $ do
    a
    drawElements Triangles (ib^.bufferSize)

instance Freeable (MeshBuffer a) where
    free (NonIndexedBuffer b) = free b
    free (IndexedBuffer b1 b2) = free b1 >> free b2
