{-# LANGUAGE TemplateHaskell #-}
module Render.Object
    ( Object, mesh, transform, fromMeshAndTransform, fromMesh, meshBuffer, transformBuffer
    , fromConvex ) where

import Common hiding (show)
import Prelude (Show(..))
import Geometry.Transform
import Render.Mesh (Mesh(Indexed))
import Render.MeshBuffer (MeshBuffer)
import Render.Vertex
import Linear hiding (_x,  _y, V2)
import Graphics.Freeable

data Object a = Object { _transform       :: Transform a
                       , _transformBuffer :: Maybe (M44 a)
                       , _mesh            :: Mesh a
                       , _meshBuffer      :: Either Garbage (MeshBuffer a) }

makeLensesFor [ ("_meshBuffer", "meshBuffer")
              , ("_transformBuffer", "transformBuffer") ] ''Object

instance Eq a => Eq (Object a) where
    o1 == o2 = _transform o1 == _transform o2 && _mesh o1 == _mesh o2

instance Ord a => Ord (Object a) where
    compare o1 o2 = compare (_transform o1) (_transform o2) <> compare (_mesh o1) (_mesh o2)

instance Show a => Show (Object a) where
    show o = "Object { _transform = (" ++ pshow (_transform o) ++ "), "
          ++ "_mesh = (" ++ pshow (_mesh o) ++ ") }"

mesh :: Eq a => Lens' (Object a) (Mesh a)
mesh f obj = upd <$> f oldMesh
    where oldMesh = _mesh obj
          upd m | m == oldMesh = obj
                | otherwise    = obj { _mesh = m, _meshBuffer = newBuff }
          newBuff = case _meshBuffer obj of
              Right mb -> Left (Garbage mb)
              Left g   -> Left g

transform :: Eq a => Lens' (Object a) (Transform a)
transform f obj = upd <$> f oldTransform
    where oldTransform = _transform obj
          upd t | t == oldTransform = obj
                | otherwise         = obj { _transform = t, _transformBuffer = Nothing }

fromMeshAndTransform :: Mesh a -> Transform a -> Object a
fromMeshAndTransform m t = Object t Nothing m (Left (Garbage ()))

fromMesh :: Num a => Mesh a -> Object a
fromMesh m = fromMeshAndTransform m noTransform

instance Eq a => Has2DCoords (Object a) where
    type VectorField (Object a) = a
    _x = transform . _x
    _y = transform . _y

fromConvex :: (Num a) => V4 Float -> [V2 a] -> Object a
fromConvex col poly = fromMesh (Indexed verts inds)
    where verts = fmap (\(V2 x y) -> Vertex (V3 x y 0) col) poly
          (i : is) = [0..polySize - 1]
          inds = concat (zipWith (\i1 i2 -> [i, i1, i2]) is (unsafeTail is))
          polySize = length poly
