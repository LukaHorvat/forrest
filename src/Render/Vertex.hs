{-# LANGUAGE UndecidableInstances, TemplateHaskell #-}
module Render.Vertex where

import Common

import Linear

data Vertex a = Vertex { _position :: V3 a
                       , _color    :: V4 Float }
                       deriving (Eq, Ord, Read, Show, Functor)

makeLenses ''Vertex

transformVertex :: Num a => Vertex a -> M44 a -> Vertex a
transformVertex v m = v & position %~ (view _xyz . (m !*) . point)

vertexToPair :: Vertex a -> (V3 a, V4 Float)
vertexToPair (Vertex p c) = (p, c)
