module Render.Mesh
    ( Triangle(..), Mesh(..), triangleToList, trianglesToIndexed, square, transformMesh ) where

import Common hiding (for)
import Render.Vertex
import Linear
import Common
import Geometry.Transform

data Triangle a = Triangle (Vertex a) (Vertex a) (Vertex a) deriving (Eq, Ord, Read, Show, Functor)
data Mesh a = NonIndexed [Triangle a]
            | Indexed [Vertex a] [Int] deriving (Eq, Ord, Read, Show, Functor)

triangleToList :: Triangle a -> [Vertex a]
triangleToList (Triangle a b c) = [a, b, c]

trianglesToIndexed :: [Triangle a] -> Mesh a
trianglesToIndexed tris = Indexed verts [0..length verts - 1]
    where verts = concatMap triangleToList tris

instance Semigroup (Mesh a) where

instance Monoid (Mesh a) where
    mempty = NonIndexed []
    mappend (NonIndexed tris1) (NonIndexed tris2) = NonIndexed (tris1 ++ tris2)
    mappend (NonIndexed tris) other = mappend (trianglesToIndexed tris) other
    mappend other (NonIndexed tris) = mappend other (trianglesToIndexed tris)
    mappend (Indexed verts1 idxs1) (Indexed verts2 idxs2) =
        Indexed (verts1 ++ verts2) (idxs1 ++ map (+ length verts1) idxs2)

square :: Num a => V4 Float -> Mesh a
square col = Indexed verts [0, 1, 2, 0, 2, 3]
    where verts = map ($ col)
                  [ Vertex (V3 (-1) (-1) 0)
                  , Vertex (V3 1    (-1) 0)
                  , Vertex (V3 1    1    0)
                  , Vertex (V3 (-1) 1    0) ]

transformMesh :: Floating a => Mesh a -> Transform a -> Mesh a
transformMesh (NonIndexed tris) (toMatrix -> mat) =
    NonIndexed $ for tris $ \(Triangle a b c) -> Triangle (transformVertex a mat)
                                                          (transformVertex b mat)
                                                          (transformVertex c mat)
transformMesh (Indexed verts inds) (toMatrix -> mat) =
    Indexed (map (`transformVertex` mat) verts) inds
