{-# LANGUAGE TemplateHaskell #-}
module Control.Monad.GL.Shader where

import Prelude (String)

import Data.FileEmbed

defaultFragment :: String
defaultFragment = $(embedStringFile "src/Shaders/Fragment.glsl")

defaultVertex :: String
defaultVertex = $(embedStringFile "src/Shaders/Vertex.glsl")
