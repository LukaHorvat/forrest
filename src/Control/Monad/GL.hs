{-# LANGUAGE UndecidableInstances #-}
module Control.Monad.GL where

import Common
import Prelude (String)
import Data.List (lookup)
import qualified Graphics.GL.Compatibility33 as GL
import qualified Foreign as Ptr
import qualified Foreign.C.String as Ptr
import Graphics.GL.Types
import Linear

data ShaderType = VertexShader | FragmentShader deriving (Eq, Ord, Read, Show)
data NumComponents = Vec1 | Vec2 | Vec3 | Vec4 | BGRA deriving (Eq, Ord, Read, Show)
newtype GLIdentifier = GLIdentifier GLuint deriving (Eq, Ord, Read, Show)
data DrawMode = Triangles deriving (Eq, Ord, Read, Show)
data BufferType = ArrayBuffer | IndexBuffer deriving (Eq, Ord, Read, Show)
data ClearBit = ColorBufferBit | DepthBufferBit deriving (Eq, Ord, Read, Show)
data GLError = NoError | InvalidEnum | InvalidValue | InvalidOperation | InvalidFramebufferOperation
             | OutOfMemory | StackUnderflow | StackOverflow deriving (Eq, Ord, Read, Show, Enum)
data EnableFlag = DepthTest | Multisample deriving (Eq, Ord, Read, Show, Enum)

class Monad m => MonadGL m where
    genBuffers :: Int -> m [GLIdentifier]
    deleteBuffers :: [GLIdentifier] -> m ()
    bindBuffer :: BufferType -> Maybe GLIdentifier -> m ()
    bufferData :: GLIdentifier -> [Float] -> m ()
    indexData :: GLIdentifier -> [Int]    -> m ()
    createShader :: ShaderType -> m GLIdentifier
    shaderSource :: GLIdentifier -> String -> m ()
    compileShader :: GLIdentifier -> m ()
    getShaderInfo :: GLIdentifier -> m String
    createProgram :: m GLIdentifier
    deleteProgram :: GLIdentifier -> m ()
    attachShader :: GLIdentifier -> GLIdentifier -> m ()
    linkProgram :: GLIdentifier -> m ()
    useProgram :: Maybe GLIdentifier -> m ()
    getAttribLocation :: GLIdentifier -> String -> m (Maybe GLIdentifier)
    vertexAttribPointer :: GLIdentifier -> NumComponents -> ("stride" ::: Int) -> ("offset" ::: Int) -> m ()
    enableVertexAttribArray :: GLIdentifier -> m ()
    disableVertexAttribArray :: GLIdentifier -> m ()
    genVertexArrays :: Int -> m [GLIdentifier]
    deleteVertexArrays :: [GLIdentifier] -> m ()
    bindVertexArray :: GLIdentifier -> m ()
    unbindVertexArray :: m ()
    drawArrays :: DrawMode -> Int -> Int -> m ()
    drawElements :: DrawMode -> Int -> m ()
    clearColor :: Float -> Float -> Float -> Float -> m ()
    clear :: ClearBit -> m ()
    bindFragDataLocation :: GLIdentifier -> Int -> String -> m ()
    getError :: m GLError
    getUniformLocation :: GLIdentifier -> String -> m (Maybe GLIdentifier)
    uniformMatrix4 :: GLIdentifier -> M44 Float -> m ()
    enable :: EnableFlag -> m ()

floatSize :: Int
floatSize = Ptr.sizeOf (undefined :: Float)

intSize :: Int
intSize = Ptr.sizeOf (undefined :: Int)

v4ToList :: V4 a -> [a]
v4ToList (V4 x y z w) = [x, y, z, w]

m44ToList :: M44 a -> [a]
m44ToList = concatMap v4ToList . v4ToList

intToVoidPtr :: Int -> Ptr.Ptr ()
intToVoidPtr = Ptr.intPtrToPtr . fromIntegral

instance MonadGL IO where
    genBuffers size = Ptr.allocaArray size (\p -> do
        GL.glGenBuffers gsize p
        coerce <$> Ptr.peekArray size p)
        where gsize = fromIntegral size

    deleteBuffers ids = Ptr.withArrayLen (coerce ids) (\l ptr ->
        GL.glDeleteBuffers (fromIntegral l) ptr)

    bindBuffer buffType buff = case buff of
        Nothing -> GL.glBindBuffer buffType' 0
        Just b  -> GL.glBindBuffer buffType' (coerce b)
        where buffType' = case buffType of
                  ArrayBuffer -> GL.GL_ARRAY_BUFFER
                  IndexBuffer -> GL.GL_ELEMENT_ARRAY_BUFFER

    bufferData buff list = do
        GL.glBindBuffer GL.GL_ARRAY_BUFFER (coerce buff)
        Ptr.withArrayLen list (\s p ->
            GL.glBufferData GL.GL_ARRAY_BUFFER (sizeFloats s) (Ptr.castPtr p) GL.GL_STATIC_DRAW)
        GL.glBindBuffer GL.GL_ARRAY_BUFFER 0
        where sizeFloats n = fromIntegral $ n * floatSize

    indexData buff list = do
        GL.glBindBuffer GL.GL_ELEMENT_ARRAY_BUFFER (coerce buff)
        Ptr.withArrayLen (map fromIntegral list :: [GLuint]) (\s p ->
            GL.glBufferData GL.GL_ELEMENT_ARRAY_BUFFER (sizeInts s) (Ptr.castPtr p) GL.GL_STATIC_DRAW)
        GL.glBindBuffer GL.GL_ELEMENT_ARRAY_BUFFER 0
        where sizeInts n = fromIntegral $ n * intSize

    createShader VertexShader = coerce <$> GL.glCreateShader GL.GL_VERTEX_SHADER
    createShader FragmentShader = coerce <$> GL.glCreateShader GL.GL_FRAGMENT_SHADER

    shaderSource shaderId src =
        Ptr.withCString src $ \srcPtr ->
            Ptr.withArray [srcPtr] $ \srcPtrPtr ->
                GL.glShaderSource (coerce shaderId) 1 srcPtrPtr Ptr.nullPtr

    compileShader = GL.glCompileShader . coerce

    getShaderInfo shaderId = do
        infoLogSize <- Ptr.alloca $ \infoLogSizePtr -> do
            GL.glGetShaderiv (coerce shaderId) GL.GL_INFO_LOG_LENGTH infoLogSizePtr
            Ptr.peek infoLogSizePtr
        if infoLogSize == 0 then return ""
        else Ptr.allocaArray (fromIntegral infoLogSize) $ \logPtr -> do
            GL.glGetShaderInfoLog (coerce shaderId) infoLogSize Ptr.nullPtr logPtr
            Ptr.peekCString logPtr

    createProgram = coerce <$> GL.glCreateProgram

    deleteProgram pid = GL.glDeleteProgram (coerce pid)

    attachShader prog shad = GL.glAttachShader (coerce prog) (coerce shad)

    linkProgram = GL.glLinkProgram . coerce

    useProgram Nothing = GL.glUseProgram 0
    useProgram (Just i) = GL.glUseProgram (coerce i)

    getAttribLocation prog attrib = do
        loc <- Ptr.withCString attrib (GL.glGetAttribLocation (coerce prog))
        return $! if loc < 0 then Nothing else Just (GLIdentifier (fromIntegral loc))

    vertexAttribPointer attrib numComps stride offset =
        GL.glVertexAttribPointer (coerce attrib) numComps' GL.GL_FLOAT GL.GL_FALSE s o
        where numComps' = case numComps of
                  Vec1 -> 1
                  Vec2 -> 2
                  Vec3 -> 3
                  Vec4 -> 4
                  BGRA -> GL.GL_BGRA
              s = fromIntegral (stride * floatSize)
              o = intToVoidPtr (offset * floatSize)

    enableVertexAttribArray = GL.glEnableVertexAttribArray . coerce

    disableVertexAttribArray = GL.glDisableVertexAttribArray . coerce

    genVertexArrays num = Ptr.allocaArray num $ \arr -> do
        GL.glGenVertexArrays (fromIntegral num) arr
        coerce <$> Ptr.peekArray num arr

    deleteVertexArrays ids = Ptr.withArrayLen (coerce ids) $ \l ptr ->
        GL.glDeleteVertexArrays (fromIntegral l) ptr

    bindVertexArray = GL.glBindVertexArray . coerce

    unbindVertexArray = GL.glBindVertexArray 0

    drawArrays Triangles start size =
        GL.glDrawArrays GL.GL_TRIANGLES (fromIntegral start) (fromIntegral size)

    drawElements Triangles size =
        GL.glDrawElements GL.GL_TRIANGLES (fromIntegral size) GL.GL_UNSIGNED_INT Ptr.nullPtr

    clearColor r g b a = GL.glClearColor (realToFrac r) (realToFrac g) (realToFrac b) (realToFrac a)

    clear ColorBufferBit = GL.glClear GL.GL_COLOR_BUFFER_BIT
    clear DepthBufferBit = GL.glClear GL.GL_DEPTH_BUFFER_BIT

    bindFragDataLocation prog col name =
        Ptr.withCString name (GL.glBindFragDataLocation (coerce prog) (fromIntegral col))

    getError = fromMaybe (error "Invalid error code") . toError <$> GL.glGetError
        where toError = flip lookup [ (GL.GL_NO_ERROR, NoError)
                                    , (GL.GL_INVALID_ENUM, InvalidEnum)
                                    , (GL.GL_INVALID_VALUE, InvalidValue)
                                    , (GL.GL_INVALID_OPERATION, InvalidOperation)
                                    , (GL.GL_INVALID_FRAMEBUFFER_OPERATION, InvalidFramebufferOperation)
                                    , (GL.GL_OUT_OF_MEMORY, OutOfMemory)
                                    , (GL.GL_STACK_UNDERFLOW, StackUnderflow)
                                    , (GL.GL_STACK_OVERFLOW, StackOverflow) ]

    getUniformLocation prog name = do
        loc <- Ptr.withCString name (GL.glGetUniformLocation (coerce prog))
        return $! if loc < 0 then Nothing else Just (GLIdentifier (fromIntegral loc))

    uniformMatrix4 (GLIdentifier loc) mat =
        Ptr.withArray dat (GL.glUniformMatrix4fv (fromIntegral loc) 1 GL.GL_TRUE)
        where dat = m44ToList mat

    enable DepthTest = GL.glEnable GL.GL_DEPTH_TEST
    enable Multisample = GL.glEnable GL.GL_MULTISAMPLE

(.:.) :: (t3 -> t2) -> (t1 -> t -> t3) -> t1 -> t -> t2
(f .:. g) x y = f (g x y)

instance {-# OVERLAPPABLE #-} (MonadTrans t, Monad (t m), MonadGL m) => MonadGL (t m) where
    genBuffers = lift . genBuffers
    deleteBuffers = lift . deleteBuffers
    bindBuffer = lift .:. bindBuffer
    bufferData = lift .:. bufferData
    indexData = lift .:. indexData
    createShader = lift . createShader
    shaderSource = lift .:. shaderSource
    compileShader = lift . compileShader
    getShaderInfo = lift . getShaderInfo
    createProgram = lift createProgram
    deleteProgram = lift . deleteProgram
    attachShader = lift .:. attachShader
    linkProgram = lift . linkProgram
    useProgram = lift . useProgram
    getAttribLocation = lift .:. getAttribLocation
    vertexAttribPointer i c s o = lift (vertexAttribPointer i c s o)
    enableVertexAttribArray = lift . enableVertexAttribArray
    disableVertexAttribArray = lift . disableVertexAttribArray
    genVertexArrays = lift . genVertexArrays
    deleteVertexArrays = lift . deleteVertexArrays
    bindVertexArray = lift . bindVertexArray
    unbindVertexArray = lift unbindVertexArray
    drawArrays m a b = lift (drawArrays m a b)
    drawElements = lift .:. drawElements
    clearColor x y z w = lift (clearColor x y z w)
    clear = lift . clear
    bindFragDataLocation i n s = lift (bindFragDataLocation i n s)
    getError = lift getError
    getUniformLocation = lift .:. getUniformLocation
    uniformMatrix4 = lift .:. uniformMatrix4
    enable = lift . enable
