{-# LANGUAGE RecordWildCards, TemplateHaskell, OverloadedStrings, RankNTypes, PatternSynonyms #-}
module Game ( simulate, Game, game, simulation, display, FrameInfo(..), frameTime, totalTime
            , crashIfError, module X, pattern KeyPressed, pattern KeyReleased ) where

import Common
import Data.IORef

import qualified SDL.Video as SDL
import qualified SDL.Time as SDL
import qualified SDL.Event as SDL
import qualified SDL.Input.Keyboard as SDL
import qualified SDL.Init as SDL
import Linear hiding (translation, V2(..))
import SDL.Event as X
import SDL.Input.Keyboard.Codes as X
import qualified SDL.Raw.Video as Raw
import qualified SDL.Raw.Enum as Raw
import Game.Effect

import Control.Monad.GL
import Control.Monad.GL.Shader
import Render.Object
import Render.MeshBuffer
import qualified Geometry.Transform as Trans
import Graphics.Program
import Graphics.Freeable

perspMat :: M44 Float
perspMat = perspective (pi / 4) (800 / 500) 0.1 100

viewMat :: M44 Float
viewMat = lookAt (V3 0 0 0) (V3 0 0 (-1)) (V3 0 1 0)

data Game m t = Game
    { initial :: GameT m (t (Object Float))
    , step    :: FrameInfo -> t (Object Float) -> GameT m (t (Object Float))
    , event   :: SDL.Event -> t (Object Float) -> GameT m (t (Object Float)) }

game :: GameT m (t (Object Float))
     -> (FrameInfo -> t (Object Float) -> GameT m (t (Object Float)))
     -> (Event -> t (Object Float) -> GameT m (t (Object Float)))
     -> Game m t
game = Game

simulation :: Monad m
           => GameT m (t (Object Float))
           -> (FrameInfo -> t (Object Float) -> GameT m (t (Object Float)))
           -> Game m t
simulation i s = game i s (const return)

display :: Monad m
        => GameT m (t (Object Float))
        -> Game m t
display i = simulation i (const return)

withWindow :: MonadIO m => (SDL.Window -> m ()) -> m ()
withWindow act = do
    w <- liftIO $ do
        SDL.initializeAll
        let ctx = SDL.defaultWindow {
                SDL.windowOpenGL = Just SDL.OpenGLConfig {
                    SDL.glColorPrecision = V4 8 8 8 8
                  , SDL.glDepthPrecision = 8
                  , SDL.glStencilPrecision = 0
                  , SDL.glProfile = SDL.Compatibility SDL.Debug 3 2
                  }
              , SDL.windowInitialSize = V2 800 500
              }
        w <- SDL.createWindow "forrest" ctx
        void $ SDL.glCreateContext w
        enable Multisample
        void $ Raw.glSetAttribute Raw.SDL_GL_MULTISAMPLESAMPLES 4
        return w
    act w

data FrameInfo = FrameInfo { _totalTime :: Double
                           , _frameTime :: Double }
                           deriving (Eq, Ord, Read, Show)
makeLenses ''FrameInfo

crashIfError :: (MonadIO m, MonadGL m) => IORef Bool -> m ()
crashIfError w = do
    err <- getError
    liftIO $
        if err == NoError then putText "No error"
        else do
            print err
            writeIORef w True

simulate :: (MonadIO m, MonadGL m, Traversable t) => Game m t -> m ()
simulate Game{..} = withWindow $ \w -> handleGame $ do
    enable DepthTest
    errProg <- newProgram defaultVertex defaultFragment
    case errProg of
        Left err -> liftIO $ putStrLn err
        Right prog -> withProgram prog $ do
            Just posAttrib <- attribLocation "position" prog
            Just colAttrib <- attribLocation "vert_color" prog

            Just modelUni <- uniformLocation "model" prog
            Just viewUni <- uniformLocation "view" prog
            Just projectionUni <- uniformLocation "projection" prog

            initObjs <- initial
            doneRef <- liftIO $ newIORef False
            initialTime <- liftIO $ fromIntegral <$> SDL.ticks
            let loop fi objs = do
                    clearColor 0 0 0 1
                    clear ColorBufferBit
                    clear DepthBufferBit
                    objs' <- upload objs
                    forM_ objs' $ \obj -> do
                        let Just mat = view transformBuffer obj
                            Right mb = view meshBuffer obj
                        renderMeshBuffer mb $ do
                            uniformMatrix4 projectionUni perspMat
                            uniformMatrix4 viewUni viewMat
                            uniformMatrix4 modelUni mat
                            enableVertexAttribArray posAttrib
                            vertexAttribPointer posAttrib Vec3 7 0
                            enableVertexAttribArray colAttrib
                            vertexAttribPointer colAttrib Vec4 7 3
                    liftIO $ SDL.glSwapWindow w
                    evt <- liftIO SDL.pollEvent
                    liftIO $ modifyIORef' doneRef $ \v -> v || case evt of
                        Just (SDL.Event _ (SDL.KeyboardEvent
                            (SDL.KeyboardEventData _ SDL.Released _
                                (SDL.Keysym (SDL.Scancode 41) _ _)))) -> True
                        Just (SDL.Event _ (SDL.WindowClosedEvent _)) -> True
                        _ -> False
                    done <- liftIO $ readIORef doneRef
                    totalTime' <- liftIO SDL.ticks
                    let ms = fromIntegral totalTime'
                        newFi = FrameInfo ms (ms - fi^.totalTime)
                    newState <- step newFi =<< case evt of
                        Just evt' -> event evt' objs'
                        Nothing -> return objs'
                    unless done (loop newFi newState)
            loop (FrameInfo initialTime 16) initObjs
            liftIO $ SDL.destroyWindow w

upload :: (MonadIO m, Traversable t) => t (Object Float) -> m (t (Object Float))
upload = liftIO . mapM uploadObj
    where uploadObj obj = do
               meshBuf <- case view meshBuffer obj of
                   Left g -> do
                       free g
                       uploadMesh (view mesh obj)
                   Right buf -> return buf
               let mat = fromMaybe (Trans.toMatrix (view transform obj)) (view transformBuffer obj)
               return $! (obj & meshBuffer .~ Right meshBuf) & transformBuffer .~ Just mat

pattern KeyPressed :: Scancode -> Event
pattern KeyPressed code <-
    Event _ (KeyboardEvent (KeyboardEventData _ Pressed _ (SDL.Keysym code _ _)))

pattern KeyReleased :: Scancode -> Event
pattern KeyReleased code <-
    Event _ (KeyboardEvent (KeyboardEventData _ Released _ (SDL.Keysym code _ _)))
