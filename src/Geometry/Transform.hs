{-# LANGUAGE TemplateHaskell #-}
module Geometry.Transform where

import Common
import Linear hiding (translation)
import qualified Linear
import Geometry.Rotation

data Transform a = Transform { _pitch       :: a
                             , _yaw         :: a
                             , _roll        :: a
                             , _translation :: V3 a
                             , _scale       :: V3 a }
                             deriving (Eq, Ord, Read, Show, Functor)

makeLenses ''Transform

toMatrix :: Floating a => Transform a -> M44 a
toMatrix tran = rotScale & Linear.translation .~ (tran ^. translation)
    where rot = rotationXZ (tran ^. yaw)
            !*! rotationYZ (tran ^. pitch)
            !*! rotationXY (tran ^. roll)
          sc = scaled (tran^.scale & point)
          rotScale = rot !*! sc

noTransform :: Num a => Transform a
noTransform = Transform 0 0 0 (V3 0 0 0) (V3 1 1 1)

instance Has2DCoords (Transform a) where
    type VectorField (Transform a) = a
    _x = translation . Linear._x
    _y = translation . Linear._y

instance R1 Transform where
    _x = translation . Linear._x

instance R2 Transform where
    _y = translation . Linear._y

instance R3 Transform where
    _z = translation . Linear._z
