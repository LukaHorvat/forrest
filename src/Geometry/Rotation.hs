module Geometry.Rotation where

import Common
import Linear

rotationXY :: Floating a => a -> M44 a
rotationXY a = V4 (V4 (cos a) (-sin a) 0 0)
                  (V4 (sin a) (cos a)  0 0)
                  (V4 0       0        1 0)
                  (V4 0       0        0 1)

rotationYZ :: Floating a => a -> M44 a
rotationYZ a = V4 (V4 1 0       0        0)
                  (V4 0 (cos a) (-sin a) 0)
                  (V4 0 (sin a) (cos a)  0)
                  (V4 0 0       0        1)

rotationXZ :: Floating a => a -> M44 a
rotationXZ a = V4 (V4 (cos a) 0 (-sin a) 0)
                  (V4 0       1 0        0)
                  (V4 (sin a) 0 (cos a)  0)
                  (V4 0       0 0        1)
