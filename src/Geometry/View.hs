module Geometry.View (lookAt, perspective) where

import Common hiding (_x, _y)
import Linear hiding (lookAt)

lookAt :: Floating a => V3 a -> V3 a -> V3 a -> M44 a
lookAt eye center up =
    V4 (V4 (xa^._x)  (xa^._y)  (xa^._z)  xd)
       (V4 (ya^._x)  (ya^._y)  (ya^._z)  yd)
       (V4 (-za^._x) (-za^._y) (-za^._z) zd)
       (V4 0         0         0          1)
    where za = signorm $ center - eye
          xa = signorm $ cross za up
          ya = cross xa za
          xd = -dot xa eye
          yd = -dot ya eye
          zd = dot za eye
