{-# LANGUAGE NoMonomorphismRestriction, TypeInType #-}
module Space.Physics
    ( module Space.Physics
    , module Space.Physics.Types
    , MonadPhysics, handlePhysics ) where

import Common

import qualified Physics.Hipmunk as H
import Linear (V4)
import qualified Data.Set as Set
import Data.IORef

import Render.Object

import Space.GameObject
import Space.Physics.Types
import Space.Physics.Effect

recenterPoly :: [V2 Double] -> [V2 Double]
recenterPoly (fmap toVector -> poly) = fmap (toVector . subtract center) poly
    where center = H.polyCenter poly

newObject :: MonadPhysics m
          => ("mass" ::: Double) -> [V2 Double] -> vis -> m (GameObject vis)
newObject m (fmap toVector -> poly) vis = do
    when (polySize < 3) $ error "The polygon needs at least 3 points"
    b <- liftHipmunk (H.newBody m mom)
    s <- liftHipmunk (H.newShape b (H.Polygon poly) center)
    space <- getSpace
    liftHipmunk (H.spaceAdd space b)
    liftHipmunk (H.spaceAdd space s)
    return (GameObject s vis)
    where center = H.polyCenter poly
          mom    = H.momentForPoly m poly 0
          polySize = length poly

newObjectWithVisual :: (MonadPhysics m, Fractional a)
                    => ("mass" ::: Double) -> [V2 Double] -> ("color" ::: V4 Float)
                    -> m (GameObject (Object a))
newObjectWithVisual m poly col = newObject m poly (fromConvex col apoly)
    where cpoly = recenterPoly poly
          apoly = fmap (fmap realToFrac) cpoly

step :: MonadPhysics m => H.Time -> m ()
step time = do
    space <- getSpace
    liftHipmunk (H.step space time)

updateVisual :: ( HasObject b (Object a), HasBody b, MonadPhysics m
                , Fractional a, Eq a )
             => b -> m b
updateVisual go = do
    ang <- liftHipmunk (getSV (H.angle bdy))
    V2 x y <- liftHipmunk (getSV (H.position bdy))
    return $ go & object %~ \o ->
        o & rotation .~ realToFrac ang
          & _x   .~ realToFrac x
          & _y   .~ realToFrac y
    where bdy = go^.body

pushObject :: (MonadPhysics m, HasBody o)
           => o -> ("force" ::: V2 Double) -> ("local offset" ::: V2 Double) -> m ()
pushObject obj forc offs = liftHipmunk $
    H.applyImpulse (obj^.body)
                   (H.Vector (forc^._x) (forc^._y))
                   (H.Vector (offs^._x) (offs^._y))

nocollide :: (HasShape o1, HasShape o2, MonadPhysics m) => o1 -> o2 -> m ()
nocollide obj1 obj2 = do
    ref <- getNocollides
    liftHipmunk $ modifyIORef' ref ( Set.insert (obj1^.shape, obj2^.shape)
                                   . Set.insert (obj2^.shape, obj1^.shape) )

weldObjects :: (MonadPhysics m, HasBody o1, HasBody o2, HasShape o1, HasShape o2)
            => o1 -> o2 -> m Weld
weldObjects obj1 obj2 = do
    space <- getSpace
    weld <- liftHipmunk $ do
        pos1 <- H.localToWorld p1b 0 >>= H.worldToLocal p2b
        pos2 <- H.localToWorld p2b 0 >>= H.worldToLocal p1b

        constr <- H.newConstraint p1b p2b (H.Pin 0 pos1)
        H.spaceAdd space constr

        constr' <- H.newConstraint p1b p2b (H.Pin pos2 0)
        H.spaceAdd space constr'
        return (Weld constr constr')
    nocollide obj1 obj2
    return weld
    where p1b = obj1^.body
          p2b = obj2^.body

motorConnectObjects :: (MonadPhysics m, HasBody o1, HasShape o1, HasBody o2, HasShape o2)
                    => o1 -> o2 -> Double -> Double -> V2 Double -> m Motor
motorConnectObjects obj1 obj2 minAng maxAng p = do
    space <- getSpace
    motor <- liftHipmunk $ do
        pos1 <- H.worldToLocal p1b (toVector p)
        pos2 <- H.worldToLocal p2b (toVector p)

        pin <- H.newConstraint p1b p2b (H.Pin pos1 pos2)
        H.spaceAdd space pin

        motor <- H.newConstraint p1b p2b (H.SimpleMotor 0)
        H.spaceAdd space motor

        limit <- H.newConstraint p1b p2b (H.RotaryLimit minAng maxAng)
        H.spaceAdd space limit
        return (Motor motor limit pin)
    nocollide obj1 obj2
    return motor
    where p1b = obj1^.body
          p2b = obj2^.body

setMotorRate :: MonadPhysics m => Double -> Motor -> m ()
setMotorRate rate mot = liftHipmunk (H.redefineC (mot^.motorMotor) (H.SimpleMotor rate))

setHipmunk :: MonadPhysics m => (o -> StateVar a) -> a -> o -> m ()
setHipmunk h a o = liftHipmunk (h o $= a)

getHipmunk :: MonadPhysics m => (o -> StateVar a) -> o -> m a
getHipmunk h o = liftHipmunk (getSV (h o))

setPosition :: (HasBody o, MonadPhysics m) => V2 Double -> o -> m ()
setPosition (toVector -> vec) = setHipmunk (H.position . view body) vec

getPosition :: (HasBody o, MonadPhysics m) => o -> m (V2 Double)
getPosition = fmap toVector . getHipmunk (H.position . view body)

setElasticity :: (HasShape o, MonadPhysics m) => Double -> o -> m ()
setElasticity = setHipmunk (H.elasticity . view shape)

getElasticity :: (HasShape o, MonadPhysics m) => o -> m Double
getElasticity = getHipmunk (H.elasticity . view shape)

setRotation :: (HasBody o, MonadPhysics m) => Double -> o -> m ()
setRotation = setHipmunk (H.angle . view body)

getRotation :: (HasBody o, MonadPhysics m) => o -> m Double
getRotation = getHipmunk (H.angle . view body)

setTorque :: (HasBody o, MonadPhysics m) => Double -> o -> m ()
setTorque = setHipmunk (H.torque . view body)

getTorque :: (HasBody o, MonadPhysics m) => o -> m Double
getTorque = getHipmunk (H.torque . view body)
