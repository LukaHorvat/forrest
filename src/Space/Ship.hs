module Space.Ship where

import Common

import Linear (_z)
import Space.GameObject
import Render.Object
import Render.Color
import Geometry.Transform

import Space.Physics

data PartKind = Hull | Thruster | SomePart
newtype ShipPart (part :: PartKind) obj = ShipPart { _gameObject :: GameObject obj }
                                        deriving (Eq,  Ord, Functor, Foldable, Traversable)
makeLenses ''ShipPart
instance HasObject (GameObject obj) obj => HasObject (ShipPart k obj) obj where
    object = gameObject . object
instance HasBody (ShipPart k obj) where
    body = gameObject . body
instance HasShape (ShipPart k obj) where
    shape = gameObject . shape

forgetPartKind :: ShipPart k a -> ShipPart 'SomePart a
forgetPartKind = coerce

newHullPart :: (MonadPhysics m, Fractional a, Eq a) => m (ShipPart 'Hull (Object a))
newHullPart = do
    obj <- newObjectWithVisual 100 poly white
    return $ ShipPart $ obj & visualComponent . transform . translation . _z .~ -50
    where poly = [ V2 (-1)   1
                 , V2   1    1
                 , V2   1  (-1)
                 , V2 (-1) (-1) ]

newThruster :: (MonadPhysics m, Fractional a, Eq a) => m (ShipPart 'Thruster (Object a))
newThruster = do
    obj <- newObjectWithVisual 10 poly red
    return $ ShipPart $ obj & visualComponent . transform . translation . _z .~ -50
    where poly = [ V2 (-1)   1
                 , V2   1    1
                 , V2   1  (-1)
                 , V2 (-1) (-1) ]

thrust :: MonadPhysics m => ("force" ::: Double) -> ShipPart 'Thruster a -> m ()
thrust f part = do
    angle <- getRotation part
    pushObject part (V2 (f * cos angle) (f * sin angle)) 0
