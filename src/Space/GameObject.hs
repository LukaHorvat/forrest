{-# LANGUAGE DeriveFoldable, DeriveTraversable #-}
module Space.GameObject where

import Common hiding (get)

import Physics.Hipmunk

import Geometry.Transform
import Render.Object

data GameObject vis = GameObject { _physicsComponent :: Shape
                                 , _visualComponent  :: vis }
                           deriving (Eq, Ord, Functor, Foldable, Traversable)
makeLenses ''GameObject

class HasRotation o where
    type RotationField o :: *
    rotation :: Lens' o (RotationField o)

instance HasRotation (Transform a) where
    type RotationField (Transform a) = a
    rotation = roll

instance Eq a => HasRotation (Object a) where
    type RotationField (Object a) = a
    rotation = transform . rotation

class HasObject o a | o -> a where
    object :: Lens' o a

instance HasObject (GameObject (Object a)) (Object a) where
    object = visualComponent
