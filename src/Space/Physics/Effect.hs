module Space.Physics.Effect
    ( MonadPhysics, PhysicsT, handlePhysics, getSpace, getNocollides, liftHipmunk )  where

import Common

import Control.Effects.Reader
import qualified Physics.Hipmunk as H
import Data.Set (Set)
import qualified Data.Set as Set
import Data.IORef

type Physics = ReadEnv (H.Space, IORef (Set (H.Shape, H.Shape)))
data Hipmunk
type instance EffectMsg1 Hipmunk = IO
type instance EffectRes1 Hipmunk = Identity
type instance EffectCon1 Hipmunk a = ()

type MonadPhysics m = (MonadEffect Physics m, MonadEffect1 Hipmunk m)
type PhysicsT m = EffectHandler Physics (EffectHandler1 Hipmunk m)

handlePhysics :: MonadIO m => PhysicsT m a -> m a
handlePhysics a = do
    liftIO H.initChipmunk
    space <- liftIO H.newSpace
    liftIO (H.step space 1) -- for some reason things glitch if we don't
                            -- do this
    nocollides <- liftIO (newIORef Set.empty)
    let checkNocollide = do
            shapes <- H.shapes
            m <- liftIO (readIORef nocollides)
            return (not (Set.member shapes m))
    liftIO (H.setDefaultCollisionHandler space
        (H.Handler (Just checkNocollide) Nothing Nothing Nothing))
    handleEffect1 ((Identity <$>) . liftIO) $ handleReadEnv (return (space, nocollides)) a

getSpace :: MonadPhysics m => m H.Space
getSpace = do
    (s :: H.Space, _ :: IORef (Set (H.Shape, H.Shape))) <- readEnv
    return s

getNocollides :: MonadPhysics m => m (IORef (Set (H.Shape, H.Shape)))
getNocollides = do
    (_ :: H.Space, r :: IORef (Set (H.Shape, H.Shape))) <- readEnv
    return r

liftHipmunk :: MonadEffect1 Hipmunk m => IO a -> m a
liftHipmunk = (runIdentity <$>) . effect1 (Proxy :: Proxy Hipmunk)
