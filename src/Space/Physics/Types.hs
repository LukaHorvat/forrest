module Space.Physics.Types where

import Common

import qualified Physics.Hipmunk as H
import Control.Lens (Getter, to)

import Space.GameObject
import Space.Physics.Effect

data PhysicsRef a = PhysicsRef
    { _setValue :: forall m. MonadPhysics m => a -> m ()
    , _getValue :: forall m. MonadPhysics m => m a
    }
makeLenses ''PhysicsRef

data Weld = Weld { _weldPin1 :: H.Constraint H.Pin
                 , _weldPin2 :: H.Constraint H.Pin }
                 deriving (Eq, Ord)
makeLenses ''Weld

data Motor = Motor { _motorMotor :: H.Constraint H.SimpleMotor
                   , _motorLimit :: H.Constraint H.RotaryLimit
                   , _motorPin   :: H.Constraint H.Pin }
                   deriving (Eq, Ord)
makeLenses ''Motor

class HasBody o where
    body :: Getter o H.Body

instance HasBody H.Body where
    body = to identity

instance HasBody H.Shape where
    body = to H.body

instance HasBody (GameObject vis) where
    body = physicsComponent . body


class HasShape o where
    shape :: Getter o H.Shape

instance HasShape H.Shape where
    shape = to identity

instance HasShape (GameObject vis) where
    shape = physicsComponent
