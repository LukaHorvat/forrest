{-# LANGUAGE FlexibleContexts, PartialTypeSignatures #-}
module Main where

import Common hiding (get, group)

import Data.List (lookup)

import Game
import Game.Effect
import Space.Ship
import Space.Physics

newtype GameState obj = GameState { _gameObjectList :: [ShipPart 'SomePart obj] }
                        deriving (Eq, Ord, Functor, Foldable, Traversable)
makeLenses ''GameState

unsafeLookup :: Eq a => a -> [(a, b)] -> b
unsafeLookup x xs = let Just a = lookup x xs in a

main :: IO ()
main = handlePhysics $ do
    thruster <- newThruster
    thruster & setPosition (V2 (-2) 0)

    hulls <- forM ((,) <$> [0..5] <*> [0..5]) $ \(i, j) -> do
        hull <- newHullPart
        hull & setPosition (V2 (i * 2) (j * 2))
        nocollide hull thruster
        return ((i, j), hull)
    forM_ ((,) <$> [0..4] <*> [0..4]) $ \(i, j) -> do
        let h = unsafeLookup (i, j) hulls
            hs = map (`unsafeLookup` hulls)
                     [ (i + x, j + y) | x <- [0, 1], y <- [0, 1]
                                      , x /= 0 || y /= 0 ]
        mapM_ (weldObjects h) hs

    motor <- motorConnectObjects (snd (unsafeHead hulls)) thruster (-1) 1 (V2 (-1) 0)

    simulate $ simulation
        (return (GameState ( fmap (forgetPartKind . snd) hulls
                          ++ [forgetPartKind thruster] )))
        (\ts s -> do
            whenM (isKeyDown ScancodeUp) (thruster & thrust 5)
            left  <- isKeyDown ScancodeLeft
            right <- isKeyDown ScancodeRight
            if left then motor & setMotorRate 1
            else if right then motor & setMotorRate (-1)
            else motor & setMotorRate 0

            step (ts^.frameTime / 3000) --(1 / 1000)
            newObjects <- mapM updateVisual (s^.gameObjectList)
            return (GameState newObjects))
