{-# LANGUAGE TemplateHaskell #-}
module Graphics.Buffer where

import Common
import Control.Monad.GL
import Linear
import Render.Vertex
import Graphics.Freeable

data Buffer a = Buffer { bufId :: GLIdentifier, _bufferSize :: Int } deriving (Eq, Ord, Read, Show)
makeLensesFor [("_bufferSize", "bufferSize")] ''Buffer

newBuffer :: MonadGL m => m (Buffer a)
newBuffer = do
    [i] <- genBuffers 1
    return (Buffer i 0)

newDataBuffer :: MonadGL m => [Float] -> m (Buffer Float)
newDataBuffer dat = do
    buf <- newBuffer
    bufferData (bufId buf) dat
    return (buf { _bufferSize = length dat })

newIndexBuffer :: MonadGL m => [Int] -> m (Buffer Int)
newIndexBuffer dat = do
    buf <- newBuffer
    indexData (bufId buf) dat
    return (buf { _bufferSize = length dat })

newVertexBuffer :: MonadGL m => [Vertex Float] -> m (Buffer (Vertex Float))
newVertexBuffer dat = do
    buf <- newBuffer
    bufferData (bufId buf) vertData
    return (buf { _bufferSize = length dat })
    where vertData = concatMap (\(Vertex (V3 x y z) (V4 r g b a)) -> [x, y, z, r, g, b, a]) dat

writeMatrixBuffer :: MonadGL m => M44 Float -> Buffer (M44 Float) -> m ()
writeMatrixBuffer mat buf = bufferData (bufId buf) (m44ToList mat)

withBuffer :: MonadGL m => BufferType -> Buffer a -> m () -> m ()
withBuffer mode (Buffer bid _) a = do
    bindBuffer mode (Just bid)
    a
    bindBuffer mode Nothing

instance Freeable (Buffer a) where
    free = deleteBuffers . return . bufId
