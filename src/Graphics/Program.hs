module Graphics.Program where

import Common
import Prelude (String)
import Control.Monad.GL
import Graphics.Freeable

data Program = Program { progId :: GLIdentifier } deriving (Eq, Ord, Read, Show)

type CompileError = String

newProgram :: MonadGL m => String -> String -> m (Either CompileError Program)
newProgram vert frag = do
    vertShader <- createShader VertexShader
    shaderSource vertShader vert
    compileShader vertShader

    fragShader <- createShader FragmentShader
    shaderSource fragShader frag
    compileShader fragShader

    vertErr <- getShaderInfo vertShader
    fragErr <- getShaderInfo fragShader
    case (vertErr, fragErr) of
        ("", "") -> do
            prog <- createProgram
            attachShader prog vertShader
            attachShader prog fragShader
            bindFragDataLocation prog 0 "out_color"
            linkProgram prog
            return (Right (Program prog))
        _ -> return (Left (vertErr ++ "\n" ++ fragErr))

withProgram :: MonadGL m => Program -> m () -> m ()
withProgram (Program pid) a = useProgram (Just pid) >> a >> useProgram Nothing

attribLocation :: MonadGL m => String -> Program -> m (Maybe GLIdentifier)
attribLocation att prog = getAttribLocation (progId prog) att

uniformLocation :: MonadGL m => String -> Program -> m (Maybe GLIdentifier)
uniformLocation uni prog = getUniformLocation (progId prog) uni

instance Freeable Program where
    free = deleteProgram . progId
