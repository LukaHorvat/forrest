module Graphics.VertexArray where

import Common
import Control.Monad.GL
import Graphics.Freeable

data VertexArray = VertexArray { vaoId :: GLIdentifier } deriving (Eq, Ord, Read, Show)

newVertexArray :: MonadGL m => m () -> m VertexArray
newVertexArray a = do
    [i] <- genVertexArrays 1
    bindVertexArray i
    a
    unbindVertexArray
    return (VertexArray i)

withVertexArray :: MonadGL m => VertexArray -> m () -> m ()
withVertexArray (VertexArray vid) a = do
    bindVertexArray vid
    a
    unbindVertexArray

instance Freeable VertexArray where
    free = deleteVertexArrays . return . vaoId
