{-# LANGUAGE GADTs #-}
module Graphics.Freeable where

import Common
import Control.Monad.GL

class Freeable r where
    free :: MonadGL m => r -> m ()

instance Freeable () where
    free _ = return ()

data Garbage = forall r. Freeable r => Garbage r

instance Freeable Garbage where
    free (Garbage r) = free r
