#version 150

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

in vec3 position;
in vec4 vert_color;

out vec4 frag_color;

void main()
{
    // t[0] = vec4(1.0, 0.0, 0.0, 0.0);
    // t[1] = vec4(0.0, 1.0, 0.0, 0.0);
    // t[2] = vec4(0.0, 0.0, 1.0, 0.0);
    // t[3] = vec4(0.0, 0.0, 0.0, 1.0);
    frag_color = vert_color;
    gl_Position = projection * view * model * vec4(position, 1.0);
}
