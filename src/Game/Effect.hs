module Game.Effect (isKeyDown, MonadGame, GameT, handleGame, module Control.Effects) where

import Common

import SDL
import Control.Effects

data Game
type MonadGame = MonadEffect1 Game
type GameT = EffectHandler1 Game
type instance EffectMsg1 Game = IO
type instance EffectRes1 Game = Identity
type instance EffectCon1 Game a = ()

liftGame :: MonadGame m => IO a -> m a
liftGame = fmap runIdentity . effect1 (Proxy :: Proxy Game)

handleGame :: MonadIO m => EffectHandler1 Game m a -> m a
handleGame = handleEffect1 (fmap Identity . liftIO)

isKeyDown :: MonadGame m => Scancode -> m Bool
isKeyDown code = liftGame $ do
    st <- getKeyboardState
    return (st code)
