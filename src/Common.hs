{-# LANGUAGE ViewPatterns, PatternSynonyms, DeriveAnyClass, TypeInType #-}
{-# OPTIONS_GHC -Wno-unused-top-binds #-}
module Common ( module X, for, (:::), pattern V2, Has2DCoords(..), Lens'
              , lens, L.V2, toVector, SV.StateVar, getSV, (SV.$=) ) where

import Interlude as X hiding (for, StackOverflow, list, to)
import Control.Monad as X hiding ((<$!>))
import qualified Linear as L
import Physics.Hipmunk (Vector(..))
import Control.Lens (Lens', lens)
import qualified Data.StateVar as SV

for :: Functor f => f a -> (a -> b) -> f b
for = flip fmap

type (name :: k) ::: a = a

data StrictPair a = StrictPair {-# UNPACK #-} !a
                               {-# UNPACK #-} !a

class Has2DCoords v where
    type VectorField v :: *
    _x :: Lens' v (VectorField v)
    _y :: Lens' v (VectorField v)

instance Has2DCoords (L.V2 a) where
    type VectorField (L.V2 a) = a
    _x = L._x
    {-# INLINE _x #-}
    _y = L._y
    {-# INLINE _y #-}

instance Has2DCoords Vector where
    type VectorField Vector = Double
    _x = lens (\(Vector x _) -> x) (\(Vector _ y) x -> Vector x y)
    {-# INLINE _x #-}
    _y = lens (\(Vector _ y) -> y) (\(Vector x _) y -> Vector x y)
    {-# INLINE _y #-}

class Has2DCoords v => Vector2 v where
    constructVector2 :: VectorField v -> VectorField v -> v
    deconstructVector2 :: v -> StrictPair (VectorField v)

instance Vector2 (L.V2 a) where
    constructVector2 = L.V2
    {-# INLINE constructVector2 #-}
    deconstructVector2 (L.V2 x y) = StrictPair x y
    {-# INLINE deconstructVector2 #-}

instance Vector2 Vector where
    constructVector2 = Vector
    {-# INLINE constructVector2 #-}
    deconstructVector2 (Vector x y) = StrictPair x y
    {-# INLINE deconstructVector2 #-}

pattern V2 :: Vector2 v => VectorField v -> VectorField v -> v
pattern V2 x y <- (deconstructVector2 -> StrictPair x y)
    where V2 x y = constructVector2 x y

getSV :: (SV.HasGetter t a, MonadIO m) => t -> m a
getSV = SV.get

toVector :: (Has2DCoords v1, Vector2 v2, VectorField v1 ~ VectorField v2)
         => v1 -> v2
toVector v = constructVector2 (v^._x) (v^._y)
